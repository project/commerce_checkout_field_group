/**
 * @file
 * Written by Henri MEDOT <henri.medot[AT]absyx[DOT]fr>
 * http://www.absyx.fr
 */

(function($) {
  'use strict';

  // Drupal behavior.
  Drupal.behaviors.commerceCheckoutFieldGroup = {
    attach: function(context, settings) {
      $('fieldset.commerce-checkout-field-group-required-fields', context).once('commerce-checkout-field-group', function() {
        var $this = $(this), $required = $this.find('.form-required');
        if ($required.length) {
          $this.find('legend span.fieldset-legend').eq(0).append(' ').append($required.eq(0).clone());
        }
      });
    }
  };

})(jQuery);
